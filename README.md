# ticketRail

## Welcome!

## Deployed Application

App is still not deployed. Mosts probably it'll be accessible on heroku server.

## Things to do before app configuration

1. Please install redis server on your computer. If you're using ubuntu it's most probably `sudo apt-get install redis-server`, in case you're using arch based linux please you can simply install redis with pacman `sudo pacman -S redis`.
2. Start redis server `systemctl start redis`

## Instalation

In order to run application:
1. create `.config.json` in main directory (in which you can find `manage.py` file) with following keys "SECRET_KEY" and "DB_PASSWORD" so it looks like:
    ```
    "SECRET_KEY": "your_secret_key",
    "DB_PASSWORD": "your_db_password"
    ```
2. create postgres database using credentials that you can find in `settings.base` and `.config.json` file.
    ```
    database name: 'ticketrail',
    username: 'ticketrail_user',
    ```
3. install dependencies from requirements file `pip install -r requirements/dev.txt`
4. make migrations `python manage.py migrate`
5. run app in dev mode `./run_app_dev_mode.sh`


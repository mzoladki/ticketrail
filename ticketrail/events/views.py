from django.shortcuts import render
from django.views.generic import ListView, DetailView, TemplateView
from events.models.event import Event


class MainView(TemplateView):
    template_name = "main_view.html"


class EventListView(ListView):
    model = Event
    template_name = "event_list.html"


class EventDetailView(DetailView):
    model = Event
    template_name = "event_detail.html"

    def get_context_data(self, **kwargs):
        context = super(EventDetailView, self).get_context_data(**kwargs)
        event = self.get_object()
        context["tickets"] = (
            event.ticket_set.filter(available=True).filter(reserved=False).count()
        )
        context["vip_tickets"] = (
            event.ticket_set.filter(available=True)
            .filter(reserved=False)
            .filter(ticket_type="VIP")
            .count()
        )
        context["regular_tickets"] = (
            event.ticket_set.filter(available=True)
            .filter(reserved=False)
            .filter(ticket_type="Regular")
            .count()
        )
        context["premium_tickets"] = (
            event.ticket_set.filter(available=True)
            .filter(reserved=False)
            .filter(ticket_type="Premium")
            .count()
        )

        return context


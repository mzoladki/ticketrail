from django.db import models


class Event(models.Model):
    name = models.CharField(max_length=120, null=True, blank=True)
    date = models.DateTimeField(blank=True, null=True)
    image = models.ImageField(upload_to='img', blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return f"id.{self.pk} {self.name}"

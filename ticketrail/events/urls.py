from django.urls import path
from events.views import EventListView, EventDetailView, MainView

urlpatterns = [
    path('', MainView.as_view(), name='main-view'),
    path('events/', EventListView.as_view(), name='event-list'),
    path('events/<int:pk>/', EventDetailView.as_view(), name='event-detail'),
]

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from tickets.models.ticket import Ticket
from events.models.event import Event
from datetime import datetime
from django_rq import get_worker
import time


class TicketBaseTest(APITestCase):
    client = APIClient()

    @classmethod
    def setUpClass(cls):
        event = Event(name="Orange Warsaw Festival", date=datetime.now(),
                      description="BESTEST EVENT EVA")
        event.save()

    def setUp(self):
        event = Event.objects.get(pk=1)
        for _ in range(20):
            ticket = Ticket(ticket_type="Regular", price=100, event=event)
            ticket.save()

        for _ in range(10):
            ticket = Ticket(ticket_type="Premium", price=200, event=event)
            ticket.save()

        for _ in range(5):
            ticket = Ticket(ticket_type="VIP", price=300, event=event)
            ticket.save()

    @classmethod
    def tearDownClass(cls):
        pass

class TicketTest(TicketBaseTest):

    def test_get_ticket_list_200(self):
        response = self.client.get(
            reverse("tickets-list")
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_ticket_list_404(self):
        response = self.client.get(
            reverse('tickets-list'), {'eventId': 100}
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_post_201(self):
        response = self.client.post(
            reverse('tickets-list'), {'event': 1, 'ticket_type': "Regular", "price": 100}, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_400_event_does_not_exist(self):
        response = self.client.post(
            reverse('tickets-list'), {'event': 100, 'ticket_type': "Regular", "price": 100}, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_reserve_every_ticket_type(self):
        post_data = {
            'event_id': 1,
            'tickets': [
                {"ticket_type": "Regular", "number": 1},
                {"ticket_type": "Premium", "number": 1},
                {"ticket_type": "VIP", "number": 1}
            ]
            }
        response = self.client.post(
            reverse('tickets-reserve'), post_data, format="json"
        )
        number_of_reserved_tickets = Ticket.objects.filter(reserved=True).count()
        self.assertEqual(number_of_reserved_tickets, 3)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['amount'], 600)
        self.assertEqual(len(response.data['tickets']), 3)

    def test_reserve_many_regular_tickets(self):
        post_data = {
            'event_id': 1,
            'tickets': [
                {"ticket_type": "Regular", "number": 2},
                {"ticket_type": "Premium", "number": 0},
                {"ticket_type": "VIP", "number": 0}
            ]
        }
        response = self.client.post(
            reverse('tickets-reserve'), post_data, format="json"
        )
        number_of_reserved_tickets = Ticket.objects.filter(
            reserved=True).count()
        self.assertEqual(number_of_reserved_tickets, 2)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['amount'], 200)
        self.assertEqual(len(response.data['tickets']), 2)

    def test_reserve_many_premium_tickets(self):
        post_data = {
            'event_id': 1,
            'tickets': [
                {"ticket_type": "Regular", "number": 0},
                {"ticket_type": "Premium", "number": 2},
                {"ticket_type": "VIP", "number": 0}
            ]
        }
        response = self.client.post(
            reverse('tickets-reserve'), post_data, format="json"
        )
        number_of_reserved_tickets = Ticket.objects.filter(
            reserved=True).count()
        self.assertEqual(number_of_reserved_tickets, 2)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['amount'], 400)
        self.assertEqual(len(response.data['tickets']), 2)

    def test_reserve_many_vip_tickets(self):
        post_data = {
            'event_id': 1,
            'tickets': [
                {"ticket_type": "Regular", "number": 0},
                {"ticket_type": "Premium", "number": 0},
                {"ticket_type": "VIP", "number": 2}
            ]
        }
        response = self.client.post(
            reverse('tickets-reserve'), post_data, format="json"
        )
        number_of_reserved_tickets = Ticket.objects.filter(
            reserved=True).count()
        self.assertEqual(number_of_reserved_tickets, 2)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['amount'], 600)
        self.assertEqual(len(response.data['tickets']), 2)

    def test_too_many_vip_tickets_requested(self):
        post_data = {
            'event_id': 1,
            'tickets': [
                {"ticket_type": "Regular", "number": 1},
                {"ticket_type": "Premium", "number": 1},
                {"ticket_type": "VIP", "number": 100}
            ]
        }
        response = self.client.post(
            reverse('tickets-reserve'), post_data, format="json"
        )
        self.assertEqual(
            response.data['non_field_errors'][0], "Only 5 VIP tickets available")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_too_many_premium_tickets_requested(self):
        post_data = {
            'event_id': 1,
            'tickets': [
                {"ticket_type": "Regular", "number": 1},
                {"ticket_type": "Premium", "number": 100},
                {"ticket_type": "VIP", "number": 1}
            ]
        }
        response = self.client.post(
            reverse('tickets-reserve'), post_data, format="json"
        )
        self.assertEqual(
            response.data['non_field_errors'][0], "Only 10 Premium tickets available")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_too_many_regular_tickets_requested(self):
        post_data = {
            'event_id': 1,
            'tickets': [
                {"ticket_type": "Regular", "number": 100},
                {"ticket_type": "Premium", "number": 1},
                {"ticket_type": "VIP", "number": 1}
            ]
        }
        response = self.client.post(
            reverse('tickets-reserve'), post_data, format="json"
        )
        self.assertEqual(
            response.data['non_field_errors'][0], "Only 20 Regular tickets available")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_no_tickets_requested(self):
        post_data = {
            'event_id': 1,
            'tickets': [
                {"ticket_type": "Regular", "number": 0},
                {"ticket_type": "Premium", "number": 0},
                {"ticket_type": "VIP", "number": 0}
            ]
        }
        response = self.client.post(
            reverse('tickets-reserve'), post_data, format="json"
        )
        self.assertEqual(response.data['tickets'], [])
        self.assertEqual(response.data['amount'], 0)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_buy_reserved_tickets(self):
        reserve_data = {
            'event_id': 1,
            'tickets': [
                {"ticket_type": "Regular", "number": 0},
                {"ticket_type": "Premium", "number": 0},
                {"ticket_type": "VIP", "number": 2}
            ]
        }

        response = self.client.post(
            reverse('tickets-reserve'), reserve_data, format="json"
        )
        buy_data = {
            "token": "token",
            "currency": "EUR",
            "amount": response.data['amount'],
            "tickets": response.data['tickets']
        }

        response = self.client.post(
            reverse("tickets-buy"), buy_data, format="json"
        )

        self.assertEqual(response.data[1], "EUR")
        self.assertEqual(response.data[0], 600)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_buy_not_reserved_tickets(self):

        reserve_data = {
            'event_id': 1,
            'tickets': [
                {"ticket_type": "Regular", "number": 0},
                {"ticket_type": "Premium", "number": 0},
                {"ticket_type": "VIP", "number": 2}
            ]
        }

        response = self.client.post(
            reverse('tickets-reserve'), reserve_data, format="json"
        )
        Ticket.objects.filter(id__in=response.data['tickets']).update(reserved=False)

        buy_data = {
            "token": "token",
            "currency": "EUR",
            "amount": response.data['amount'],
            "tickets": response.data['tickets']
        }

        response = self.client.post(
            reverse("tickets-buy"), buy_data, format="json"
        )
        self.assertEqual(
            response.data['non_field_errors'][0], "Tickets are no longer reserved!")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

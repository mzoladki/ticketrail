import django_rq
from tickets.models.ticket import Ticket
from django_rq import job
from time import sleep


@job('default')
def cancel_reservation(reservation_list):
    tickets = Ticket.objects.filter(id__in=reservation_list)
    if False in tickets.values_list('available', flat=True):
        return "Tickets already bought"
    else:
        for ticket in tickets:
            ticket.reserved = False
            ticket.save()
        return f"Tickets {reservation_list} reservation canceled"

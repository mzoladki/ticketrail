import django_rq
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import action
from django.db.models import Count
from django.shortcuts import get_list_or_404, get_object_or_404
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from events.models.event import Event
from tickets.models.ticket import Ticket
from tickets.serializers import TicketSerializer, TicketReservationSerializer, TicketBuySerializer, BasicTicketSerializer
from tickets.reservation_queue import cancel_reservation
from datetime import timedelta
from tickets.payment_gateway import PaymentGateway

payment = PaymentGateway()


class TicketViewSet(viewsets.ModelViewSet):
    queryset = Ticket.objects.all()
    serializer_class = BasicTicketSerializer

    @swagger_auto_schema(title='Ticket', manual_parameters=[
        openapi.Parameter('eventId', openapi.IN_QUERY, description="event id", type=openapi.TYPE_INTEGER),
        openapi.Parameter('ticketType', openapi.IN_QUERY, description="type of the ticket (allowed 'Premium', 'Regular', 'VIP')", type=openapi.TYPE_STRING)
        ]
    )
    def list(self, request):
        event_id = request.query_params.get('eventId', None)
        ticket_type = request.query_params.get('ticketType', None)

        if event_id:
            event = get_object_or_404(Event, id=event_id)
            self.queryset = Ticket.objects.filter(event=event)
        else:
            self.queryset = Ticket.objects.all()

        if ticket_type:
            self.queryset = self.queryset.filter(ticket_type=ticket_type)

        return super(TicketViewSet, self).list(self, request)

    @swagger_auto_schema(title='Ticket',
                         in_=openapi.IN_BODY,
                         request_body=TicketReservationSerializer
                         )
    @action(detail=False, methods=['post'])
    def reserve(self, request):

        serializer = TicketReservationSerializer(data=request.data)
        response = {"tickets": [], "amount": 0}
        scheduler = django_rq.get_scheduler('default')

        if serializer.is_valid():
            event = Event.objects.get(id=serializer.data['event_id'])
            for ticket_request in serializer.data['tickets']:
                reservation = Ticket.objects.filter(
                    ticket_type=ticket_request['ticket_type'], reserved=False, available=True, event=event)[:ticket_request['number']]
                
                response['amount'] += sum(
                    reservation.values_list('price', flat=True))
                for res in reservation:
                    response['tickets'].append(res.id)
                    res.reserved = True
                    res.save()
            scheduler.enqueue_in(
                timedelta(minutes=1), cancel_reservation, reservation_list=response['tickets'])
            return Response(response, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(title='Ticket',
                         in_=openapi.IN_BODY,
                         request_body=TicketBuySerializer)
    @action(detail=False, methods=['post'])
    def buy(self, request):
        serializer = TicketBuySerializer(data=request.data)
        if serializer.is_valid():
            try:
                payment_response = payment.charge(
                    serializer.validated_data['amount'], token=serializer.validated_data['token'], currency=serializer.validated_data['currency'])
                tickets = Ticket.objects.filter(id__in=serializer.validated_data['tickets'])
                tickets.update(available=False)
                return Response(payment_response, status=status.HTTP_200_OK)
            except Exception as e:
                return Response({'non_field_errors': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(title='Ticket', manual_parameters=[openapi.Parameter(
        'eventId', openapi.IN_QUERY, description="event id", type=openapi.TYPE_INTEGER
    )])
    @action(detail=False, methods=['get'])
    def reservation_statistics(self, request):
        event_id = request.query_params.get('eventId', None)
        
        if not event_id:
            total_number_of_tickets = Ticket.objects.all().count()
            reservation = Ticket.objects.filter(reserved=True)
        else:
            total_number_of_tickets = Ticket.objects.filter(event=get_object_or_404(Event, pk=event_id)).count()
            reservation = Ticket.objects.filter(reserved=True, event=get_object_or_404(Event, pk=event_id))

        regular = reservation.filter(ticket_type="Regular").count()
        premium = reservation.filter(ticket_type="Premium").count()
        vip = reservation.filter(ticket_type="VIP").count()

        return Response({
            "total_numer_of_tickets": total_number_of_tickets,
            "reservation": reservation.count(),
            "ratio": reservation.count()/total_number_of_tickets,
            "regular": regular,
            "premium": premium,
            "vip": vip
        }, status=status.HTTP_200_OK)

    @swagger_auto_schema(title='Ticket', manual_parameters=[openapi.Parameter(
        'eventId', openapi.IN_QUERY, description="event id", type=openapi.TYPE_INTEGER
    )])
    @action(detail=False, methods=['get'])
    def available_statistics(self, request):
        event_id = request.query_params.get('eventId', None)

        if not event_id:
            total_number_of_tickets = Ticket.objects.all().count()
            reservation = Ticket.objects.filter(reserved=False)
        else:
            total_number_of_tickets = Ticket.objects.filter(
                event=get_object_or_404(Event, pk=event_id)).count()
            reservation = Ticket.objects.filter(
                reserved=False, event=get_object_or_404(Event, pk=event_id))

        regular = reservation.filter(ticket_type="Regular").count()
        premium = reservation.filter(ticket_type="Premium").count()
        vip = reservation.filter(ticket_type="VIP").count()

        return Response({
            "total_numer_of_tickets": total_number_of_tickets,
            "available": reservation.count(),
            "ratio": reservation.count()/total_number_of_tickets,
            "regular": regular,
            "premium": premium,
            "vip": vip
        }, status=status.HTTP_200_OK)

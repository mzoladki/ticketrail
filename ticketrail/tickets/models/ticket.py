from django.db import models
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import status
from events.models.event import Event
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi


class Ticket(models.Model):
    TICKET_TYPES = (
        ("Regular", "Regular"),
        ("Premium", "Premium"),
        ("VIP", "VIP")
    )
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    ticket_type = models.CharField(
        max_length=30, choices=TICKET_TYPES, default='Regular')
    price = models.DecimalField(max_digits=10, decimal_places=2)
    available = models.BooleanField(default=True)
    reserved = models.BooleanField(default=False)

    def __str__(self):
        return f"id: {self.pk} type: {self.ticket_type} for event {self.event}"

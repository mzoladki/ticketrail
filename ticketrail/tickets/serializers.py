from rest_framework import serializers
from tickets.models.ticket import Ticket
from django.shortcuts import get_object_or_404
from events.models.event import Event


class BasicTicketSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ticket
        fields = '__all__'


class TicketBuySerializer(serializers.Serializer):
    token = serializers.CharField()
    currency = serializers.CharField()
    amount = serializers.IntegerField()
    tickets = serializers.ListField(
        child=serializers.IntegerField()
    )

    def validate(self, data):
        tickets = Ticket.objects.filter(id__in=data['tickets'])
        real_amount = sum(tickets.values_list('price', flat=True))
        if real_amount != data['amount']:
            raise serializers.ValidationError("Amount is not valid!")
        elif False in tickets.values_list('reserved', flat=True):
            raise serializers.ValidationError("Tickets are no longer reserved!")
        else:
            return data


class TicketSerializer(serializers.Serializer):
    ticket_type = serializers.ChoiceField(Ticket.TICKET_TYPES)
    number = serializers.IntegerField(required=True)

class TicketReservationSerializer(serializers.Serializer):
    event_id = serializers.IntegerField()
    tickets = TicketSerializer(many=True)

    def validate(self, data):
        event = get_object_or_404(Event, id=data['event_id'])
        for tickets in data['tickets']:
            number_of_available_tickets = Ticket.objects.filter(ticket_type=tickets['ticket_type'], reserved=False, available=True, event=event).count()
            if tickets['number'] > number_of_available_tickets:
                raise serializers.ValidationError(
                    f"Only {number_of_available_tickets} {tickets['ticket_type']} tickets available")
        return data

from ticketrail.settings.base import *

INSTALLED_APPS += ["sslserver"]
ACCOUNT_EMAIL_VERIFICATION = "none"
DEBUG = True
